const gulp = require('gulp');
const { series, parallel } = require('gulp');
const clean = require('gulp-clean');
const browserSync = require('browser-sync').create();
const nunjucksRender = require('gulp-nunjucks-render');


function cleanFolder() {
  return gulp.src('./dist/', {read: false, allowEmpty: true})
    .pipe(clean());
}

function copyFile() {
  // https://gulpjs.com/docs/en/getting-started/explaining-globs/
  return gulp.src('./src/**/*')
    .pipe(gulp.dest('./dist/'));
}

function bs(cb) {
  browserSync.init({
    server: {
      baseDir: './dist/'
    }
  });

  gulp.watch('./src/**/*', gulp.series(copyFile, reload));
  cb();
}


function reload(cb) {
  browserSync.reload();
  cb();
}

function nunjucks() {
  return gulp.src('./src/**/*.html')
    .pipe(nunjucksRender({
        envOptions: {
          autoescape: true,
          trimBlocks: true,
          lstripBlocks: true
        },
        path: './src/templates/' // String or Array
      }))
    .pipe(gulp.dest('./dist/'));
}


function defaultTask(cb) {
  // place code for your default task here
  console.log('Hola Gulp: running defaultTask from gulpfile.js');
  cb();
}

exports.default = series(defaultTask, cleanFolder, nunjucks, bs);